maintainer       "SAP"
maintainer_email "d.hague@sap.com"
license          "All rights reserved"
description      "Boots a Pi as a WiFi access point, gets user credentials and then connects to a secured WiFi network"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.0.1"

