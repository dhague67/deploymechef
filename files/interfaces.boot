auto lo

iface lo inet loopback
iface eth0 inet dhcp

allow-hotplug wlan0
#iface wlan0 inet dhcp
iface wlan0 inet static
  address 192.168.42.1
  netmask 255.255.255.0
#wpa-conf /etc/wpa.conf
iface default inet dhcp
