#!/bin/bash
#
# Note that the deployme user should be set as UID 0 (root) and this file 
#  should be set as the login shell
#
# In a future version we would lose the "deployme" password and use Chef to 
#  populate allowed-user SSH certificates for the deployme user, adding
#  convenience & security.
#
clear

echo "Wireless network? [SAP-Corporate] "
read ssid
if [[ ("$ssid" == "") ]]; then
  ssid="SAP-Corporate"
fi

echo "User id? "
read userid

unset password
prompt="Network password: "
while IFS= read -p "$prompt" -r -s -n 1 char
do
    if [[ $char == $'\0' ]]
    then
        break
    fi
    prompt='*'
    password+="$char"
done
echo ""

# Create a wpa.conf file based on the entered data
if [[ "$userid" == "" ]] ; then
  # If no username, assume WPA-PSK (private wifi)i
  sed "s%.*psk=.*$%\tpsk=\"`echo $password`\"%" wpa.conf.private > /tmp/wpa.conf
  sed -i.old "s%.*ssid=.*$%\tssid=\"`echo $ssid`\"%" /tmp/wpa.conf
else
  # else assume WPA-EAP (Enterprise)
  sed "s%.*identity=.*$%\tidentity=\"`echo $userid`\"%" wpa.conf.enterprise > /tmp/wpa.conf
  sed -i.old "s%.*password=.*$%\tpassword=\"`echo $password`\"%" /tmp/wpa.conf
  sed -i.old "s%.*ssid=.*$%\tssid=\"`echo $ssid`\"%" /tmp/wpa.conf
fi

echo Copying new /etc/network/interfaces file into place
cp interfaces.wificlient /etc/network/interfaces

clear

echo "Configuration complete - this session will now terminate"
echo ""
echo "Wait a few seconds and check the DeployMe display for futher information"

# Stop hostapd and udhcpd
/etc/init.d/udhcpd stop &> /dev/null
/etc/init.d/hostapd stop &> /dev/null

# Restart wlan0
/sbin/ifdown wlan0 &> /dev/null
/sbin/ifup wlan0 &> /dev/null

# Wait to make sure we have an IP address
while [[ "$(hostname -I)" == "" ]] ; do
    sleep 1
done

# Show new IP address
lcd/showMessage.py "$ssid" "$(hostname -I)"

# Now delete the tmp wpa.conf and copy the bootup files back into place
rm /tmp/wpa.conf

# Finally, start the DeployMe application
./deployme "$userid" "$password"

