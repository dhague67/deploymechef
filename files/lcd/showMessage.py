#!/usr/bin/python

from time import sleep
from Adafruit_I2C import Adafruit_I2C
from Adafruit_MCP230xx import Adafruit_MCP230XX
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate

import smbus
import sys


# initialize the LCD plate
# use busnum = 0 for raspi version 1 (256MB) and busnum = 1 for version 2
lcd = Adafruit_CharLCDPlate(busnum = 1)

# clear display
lcd.clear()
# hello!
lcd.message(sys.argv[1]+"\n"+sys.argv[2])
sleep(1)

# first loop, just changes the color
lcd.backlight(lcd.RED)
sleep(.5)
lcd.backlight(lcd.YELLOW)
sleep(.5)
lcd.backlight(lcd.GREEN)
sleep(.5)
lcd.backlight(lcd.TEAL)
sleep(.5)
lcd.backlight(lcd.BLUE)
sleep(.5)
lcd.backlight(lcd.VIOLET)
sleep(.5)
lcd.backlight(lcd.ON)
sleep(.5)

