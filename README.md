Description
===========

Boots a Pi as a WiFi access point, gets user credentials and then connects to a secured WiFi network.


---
Requirements
============

Platform
--------

* Raspbian Wheezy

Cookbooks
---------


---
Attributes
==========


---
Recipes
=======

default
-------


---
Usage
=====


License and Author
==================

Author:: Darren Hague (<d.hague@sap.com>)


